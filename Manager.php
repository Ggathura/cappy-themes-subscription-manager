<?php

/*
  Plugin Name: Cappy Themes Subscription Manager
  Plugin URI:  http://cappythemes.com
  Description: We will manage subscriptions made to your site. We are a great tool for <strong>Email Marketing</strong>. Always growing & finding ways to boost productivity on your site.
  Version:     1.0
  Author:      George Gathura
  Author URI:  http://cappthemes.com
  License:     GPL2
  License URI: https://www.gnu.org/licenses/gpl-2.0.html
  Domain Path: /languages
  Text Domain: cappy_tsm
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

//defaults
global $wpdb;
global $capptsm_subscribeTable;
global $charset_collate;
global $db_version;
global $capptsm_newsletters;

$charset_collate = $wpdb->get_charset_collate();
$capptsm_subscribeTable='capptsm_subscribers';
$capptsm_newsletters='capptsm_newsletters';

$db_version=1;

if (!function_exists('cappytsm_initProcedure')):

    function cappytsm_initProcedure() {
        /*
         * Setup information
         */
       
         //confirm if upgrade/installation or abort if already installed
         
        $currentVersion=  get_option('cappytsm_db_version',0);
        if(intval($currentVersion)==0):
            //create Cappy Themes Subscription tables
            global $charset_collate;
            global $capptsm_subscribeTable;
            global $capptsm_newsletters;
            
            
              $sql = "CREATE TABLE $capptsm_subscribeTable (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              emailAddress varchar(100) DEFAULT '' NOT NULL,
              unsubscribe int(3) DEFAULT 1,
              lastNewsletterDate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              UNIQUE KEY id (id)
            ) $charset_collate;";
              
              $sql2 = "CREATE TABLE $capptsm_newsletters (
              id mediumint(9) NOT NULL AUTO_INCREMENT,
              subject varchar(100) DEFAULT '' NOT NULL,
              savedContent text NOT NULL,
              providerUsed int(3) DEFAULT 1,
              deployStatus int(3) DEFAULT 1,
              newsletterDate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
              UNIQUE KEY id (id)
            ) $charset_collate;";
              
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta($sql);
            dbDelta($sql2);
            
//            $wpdb->show_errors();
//            $wpdb->print_error();
            //Default Responses
            update_option('cappytsm_sucessSubscribe','Thankyour for Subscribing to us','yes');
            
            update_option('cappytsm_failSubscribe','Unfortunately, we cannot accept any'
                    . ' subscriptions at the moment','yes');
            
            
            //Default Subscription theme --future update
            update_option('cappytsm_emailTheme','default','yes');
            update_option('cappytsm_emailMarketer','default','yes');
            
            //Default Options e.g. text-color,Background,Introduction Message

//            update_option('cappytsm_db_version', $db_version,'yes');
            update_option('cappytsm_active', true,'yes');
            
        endif;
    
    }

endif;

register_activation_hook(__FILE__, 'cappytsm_initProcedure');

if (!function_exists('cappytsm_deactivationProcedure')):

    function cappytsm_deactivationProcedure() {
        //What to do when deactivated
        update_option('cappytsm_active',false,'yes');
        //Every function in Cappy needs these to be true to work
    }

endif;


if (!function_exists('cappytsm_loadForm')):

    function cappytsm_loadForm() {
        require_once plugin_dir_path(__FILE__) . 'html/front-end/cappttsm-subscribe-form.php';
    }

endif;

if (!function_exists('capptsm_pfProcedure')):

    function capptsm_pfProcedure() {
        $subscribeBtn = filter_input(INPUT_POST, 'capptsm_subscribeBtn') != null ? true : false;
        if ($subscribeBtn) {
            $status = true;

            $emailAddressVar = filter_input(INPUT_POST, '', FILTER_VALIDATE_EMAIL);

            if (is_email($emailAddressVar)) {
                $emailAddress = $emailAddressVar;
            } else {
                $status = false;
                $message = 'The email address entered is invalid.Please re-type your email address.';
            }

            if ($status) {
                global $wpdb;
                global $capptsm_subscribeTable;
                //check if email was already registered
                $checkIfNew = $wpdb->get_row('SELECT * from $capptsm_subscribeTable'
                        . ' WHERE emailAddress="' . $emailAddress . '"', ARRAY_A);

                $checkedSize=sizeof($checkIfNew);
                
                if($checkedSize != 0) {
                    $status = false; //A record with the same email exists
                    $message = 'This email address is already a subscriber to our site.';
                }
            }

            //insert email if new 

            if ($status) {
                $wpdb->insert(
                        $capptsm_subscribeTable, array(
                    'emailAddress' => $emailAddress
                        )
                );
                $message = 'Thank you for subscribing to us.';
            }
            $response = array(
                'status' => $status,
                'message' => $message
            );
            capptsm_responseMgm($response);
        }else{
           capptsm_responseMgm(null);
        }
    }

endif;

if(!function_exists('capptsm_responseMgm')):
    function capptsm_responseMgm($response=null){
        //sets the response
        if($response!=null && is_array($response)){
            //$response must always be an array
            $status=isset($response['status'])?$response['status']:false;
            $message==isset($response['status'])?$response['status']:get_option('cappytsm_failSubscribe');
            
            $message='';
            if($status){
            $message.=' <div class="alert alert-success alert-dismissible fade in" role="alert">';    
            }else{
             $message.='<div class="alert alert-warning alert-dismissible fade in" role="alert">';   
            }
            $message='
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        '.$message.'
                    </div>';
            
            return $message;
            
        }else{
            return null;
        }
    
    }
    add_action('wp_head','capptsm_responseMgm');
endif;

register_deactivation_hook(__FILE__, 'cappytsm_deactivationProcedure');
